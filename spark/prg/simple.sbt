lazy val root = (project in file(".")).settings(
  name := "Simple_Project",
  version := "1.0",
  scalaVersion := "2.10.4"
)

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "1.4.1" % "provided",
  "org.apache.spark" % "spark-streaming-kafka_2.10" % "1.4.1",
  "org.apache.spark" % "spark-streaming_2.10" % "1.4.1" % "provided"
)

// META-INF discarding
mergeStrategy in assembly <<= (mergeStrategy in assembly) { (old) =>
  {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
  }
}
para ejecutar flume en windows:

`
docker buld -t evicent/flume flume/docker
`

`
docker run -d --name bpm2cep_flume_1 -v /c/Users/tcpsi/Desktop/flume/flume:/mnt  -P --link kafka -e FLUME_AGENT_NAME=a1 -e FLUME_CONF_FILE=/mnt/config evicent/flume
`

para ejecutar spark en windows:
`
docker run -d --name bpm2cep_master_1 -v /conf -v /c/Users/tcpsi/Desktop/flume:/tmp/data  -p 7000 -p 7001 -p 7003 -p 7004 -p 7005 -p 7006 -p 7077:7077 -p 6066:6066 -p 4040:4040 -p 8080:8080   -e SPARK_CONF_DIR=/conf --link kafka --hostname master gettyimages/spark /usr/spark/bin/spark-class org.apache.spark.deploy.master.Master -h master
`

`
docker run -d --name bpm2cep_worker_1 -v /conf -v /c/Users/tcpsi/Desktop/flume:/tmp/data  -p 7000 -p 7001 -p 7003 -p 7004 -p 7005 -p 7006 -p 7077 -p 6066 -p 4040 -p 8081:8081   -e SPARK_CONF_DIR=/conf -e SPARK_WORKER_CORES=2 -e SPARK_WORKER_MEMORY=1g -e SPARK_WORKER_PORT=8881 -e SPARK_WORKER_WEBUI_PORT=8081 --hostname worker --link kafka --link bpm2cep_master_1:master gettyimages/spark /usr/spark/bin/spark-class org.apache.spark.deploy.worker.Worker spark://master:7077
`

para ejecutar kafka vease el fichero de [notas de kafka](kafka/notas.yml)

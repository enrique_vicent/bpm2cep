Flume
====
Dentro de la carpeta flume existe una subcarpeta Docker con el dockerfile que permite conseguir la versión 1.6.0 que es la necesaria para poder utilizar Kafca

El docker-compose lo construye y monta como volumen la carpata *flume* en /mnt. Esta carpeta incluye:

- el fichero [config](flume/config), con la configuración. Actualmente estamos utilizando la entrada de tipo *exec* que hace un `cat`. Debería utilizarse un tail, pero tal como está ayuda a las pruebas
- la carpeta in es donde residen los ficheros de ejemplo
- la carpeta out es donde deja una copia del procesado, es una manera rápida de comprobar como está funcionando. La misma salida de estos ficheros está siendo enviada a Kafka.

Kafka
====
Kafka se compone de dos servidores, un zookeper que se utiliza en operaciones de lectura y de creación de colas y un servidor propiamente kafka que se utiliza directamente para las escrituras

En flume y spark se cuenta con la existencia de un topic "test" para crearlo se puede lanzar el comando:

`docker run --rm --link bpm2cep_zookeeper_1:zookeeper --link bpm2cep_kafka_1:kafka ches/kafka kafka-topics.sh --create --topic test --replication-factor 1 --partitions 1 --zookeeper zookeeper:2181`

Mas comandos disponibles para leer y borrar colas en el [fichero de notas de kafka](kafka/notas.yml)

Spark
====
## Ejecutar el servidor

Existe una imagen que puede hacer las veces de master, worker o consola

para ejecutar un master y un slave solo hay que ejecutar

`docker-compse start`

Las imagenes que se ejecutan son: [gettyimages/spark](https://registry.hub.docker.com/u/gettyimages/spark/)

Si se ejecuta en windows, ver [aqui](windows.md)

## Acceso a la consola interactiva
Habiendo ejecutado el *docker-compose* basta con:

```
docker exec -it bpm2cep_master_1 /bin/bash /usr/spark/bin/spark-shell --master spark://master:7077
```
para ejecutar la consola java/scala

## Compilar el progrma
Pl programa está codificado en scala y se puede localizar en `/spark/prg`.

para compliarlo hace falta tener instalado java 8, sbt y escala o ejecutar el comando `/spark/prg/compila.sh` que lanza un docker ([1sciencie/sbt](https://registry.hub.docker.com/u/1science/sbt/)) con un entorno de compilación.

## Ejecutar el programa
```
docker exec -it bpm2cep_master_1 /usr/spark/bin/spark-submit --class "SimpleApp" --master spark://master:7077 /tmp/data/spark/prg/target/scala-2.10/Simple_Project-assembly-1.0.jar
```
durante la ejecución de la tarea se pueden ver sus estadisticas en http://192.168.99.100:4040/jobs/

![jobs](README_files/jobs.png)

______

## todo:
* ver: http://blog.cloudera.com/blog/2015/03/exactly-once-spark-streaming-from-apache-kafka/
* update Zookeeper yourself if you want Zookeeper-based Kafka monitoring tools to show progress of the streaming application
